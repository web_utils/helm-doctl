FROM dtzar/helm-kubectl:2.16.1

ENV DOCTL_VERSION=1.20.1
ENV VELERO_VERSION=v1.4.0

WORKDIR /app

RUN apk add --no-cache curl && \
  mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2 && \
  curl -L https://github.com/digitalocean/doctl/releases/download/v${DOCTL_VERSION}/doctl-${DOCTL_VERSION}-linux-amd64.tar.gz  | tar xz && \
  cp doctl /bin/ && \
  curl -L https://github.com/vmware-tanzu/velero/releases/download/$VELERO_VERSION/velero-$VELERO_VERSION-linux-amd64.tar.gz | tar xz && \
  cp velero-v1.4.0-linux-amd64/velero /bin/ && \
  rm -r velero-v1.4.0-linux-amd64/  && \
  rm doctl
